// halaman untuk melihat seluruh products, menggunakan widget products (ListView)

import 'package:flutter/material.dart';
import '../widgets/products.dart';

class ProductsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProductsWidget();
  }
}