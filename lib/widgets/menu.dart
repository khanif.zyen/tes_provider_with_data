import 'package:flutter/material.dart';

class MenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Column(
        children: <Widget>[
          Container(
            height: 100.0,
            color: Theme.of(context).accentColor,
          ),
          ListTile(
            leading: Icon(Icons.list),
            title: Text('All Products'),
            onTap: () => Navigator.of(context).pushReplacementNamed('/'),
          ),
          ListTile(
            leading: Icon(Icons.add),
            title: Text('Create Product'),
            onTap: () => Navigator.of(context).pushNamed('/product_form'),
          ),
          ListTile(
            leading: Icon(Icons.tab),
            title: Text('Product Admin'),
            onTap: () => Navigator.of(context).pushNamed('/product_admin'),
          ),
        ],
      ),
    );
  }
}
