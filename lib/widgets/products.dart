// digunakan untuk menampilkan widget products / daftar semua product berbentuk card
import 'package:flutter/material.dart';
import '../data/products.dart';

class ProductsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (BuildContext context,int index){
        return Card(
          elevation: 10.0,
          child: Column(
            children: <Widget>[
              Image.asset(products[index].image),
              Text(products[index].title),
            ],
          ),
        );
      },
      itemCount: products.length,);
  }
}