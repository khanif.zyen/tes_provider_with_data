// initial product
import '../models/product.dart';

Product product = Product(
  title: 'Kursi Makan',
  description: 'Kursi makan minimalis, dibuat dengan sepenuh hati',
  price: 590000,
  image: 'assets/images/kursi1.jpg',
);

List<Product> products = [
  Product(
      title: 'Kursi Makan',
      description: 'Kursi makan minimalis, dibuat dengan sepenuh hati',
      price: 590000,
      image: 'assets/images/kursi1.jpg'),
  Product(
      title: 'Kursi Makan',
      description: 'Kursi makan minimalis, dibuat dengan sepenuh hati',
      price: 590000,
      image: 'assets/images/sofa1.jpg'),
];
