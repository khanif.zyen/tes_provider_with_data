import 'package:flutter/material.dart';
import 'package:tes_provider_with_data/pages/product.dart';
import 'package:tes_provider_with_data/pages/product_admin.dart';
import 'package:tes_provider_with_data/pages/product_form.dart';
import 'package:tes_provider_with_data/widgets/menu.dart';
import 'package:tes_provider_with_data/widgets/products.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final String title = 'Provider with Data Demo';
  String get getTitle => title;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      //home: HomePage(),
      routes: {
        '/': (context) => HomePage(),
        '/product_form': (context) => ProductForm(),
        '/product_admin': (context) => ProductAdmin(),
        '/product': (context) => ProductPage(),
      },
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).orientation == Orientation.portrait)
      return Scaffold(
        appBar: AppBar(
          title: Text(MyApp().getTitle),
        ),
        body: ProductsWidget(),
        drawer: Drawer(
          child: MenuWidget(),
        ),
      );
    else
      return Scaffold(
        appBar: AppBar(
          title: Text(MyApp().getTitle),
        ),
        body: Row(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width / 3,
                height: double.infinity,
                child: MenuWidget()),
            ProductsWidget(),
          ],
        ),
      );
  }
}

class Demo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int hitung;
    if (MediaQuery.of(context).orientation == Orientation.landscape)
      hitung = 4;
    else
      hitung = 2;
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider with Data Demo'),
      ),
      body: GridView.count(
        // Create a grid with 2 columns. If you change the scrollDirection to
        // horizontal, this would produce 2 rows.
        crossAxisCount: hitung,
        // Generate 100 Widgets that display their index in the List
        children: List.generate(100, (index) {
          return Container(
            decoration: BoxDecoration(color: Colors.green),
            margin: EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                'Item $index',
                style: Theme.of(context).textTheme.headline,
              ),
            ),
          );
        }),
      ),
    );
  }
}
